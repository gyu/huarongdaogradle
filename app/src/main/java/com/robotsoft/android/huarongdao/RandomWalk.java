package com.robotsoft.android.huarongdao;

import java.util.Random;

public class RandomWalk {
	private int[] data;
	private Random rnd;
	private int[][] config=new int[10][]; 

	public RandomWalk(int seed, int step, long l1, long l2) {
		data = SampleUtil.getData(l1, l2);
		rnd = new Random(seed);
		for(int i=0; i<step; i++) {
			walk();
		}
	}

	private void walk() {
		int z=0;
		for (int i = 0; i < 20; i++) {
			int j = data[i];
			if (j == 0 || j == 5)
				continue;
			if (i >= 15 && j < 3)
				continue;
			for (int k = 0; k < 4; k++) {
				int[] q = Algo.Vacancies[j - 1][i][k];
				if (q.length > 0) {
					if (data[q[0]] == 5) {
						if (q.length == 1 || data[q[1]] == 5) {
							config[z++]=new int[] { i, j - 1, k };
						}
					}
				}
			}
		}
		int n = rnd.nextInt(z);
		int[] r = config[n];
		int[][] chgs = Algo.Results[r[1]][r[0]][r[2]];
		for (int i = 0; i < chgs.length; i++) {
			data[chgs[i][0]] = chgs[i][1];
		}
	}
	
	public long[] getValues() {
		return SampleUtil.getPattern(data);
	}
}
