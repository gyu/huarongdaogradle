package com.robotsoft.android.huarongdao;

import com.robotsoft.android.base.BackgroundFactory;
import com.robotsoft.android.base.Clock;
import com.robotsoft.android.base.UIPreference;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class StatsPanel extends Activity {
	
	private StatsItem[] items;
	private int[][] marks;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stats_panel);
        Database db = new Database(this);
        items = db.getStats();
        TableLayout table = (TableLayout)findViewById(R.id.stats_table);
        int n=items==null?0:items.length;
        int[] q=new int[4];
        marks=new int[4][5];
        for(int i=0; i<n; i++) {
        	int j=items[i].getType();
        	int m=q[j];
        	q[j]=m+1;
        	marks[j][m]=i;
        }
        for(int i=0; i<4; i++) {
        	if(q[i]==0) {
        		table.removeView(caption(i));
        		table.removeView(header(i));
        		for(int j=0; j<5; j++) {
        			table.removeView(content(i,j));
        		}
        	}
        	else {
        		for(int j=0; j<5; j++) {
        			TableRow rw=content(i,j);
        			if(j<q[i]) {
        				TextView txt=(TextView)rw.getChildAt(0);
        				txt.setText(String.valueOf(items[marks[i][j]].getMoves().length));
        				txt=(TextView)rw.getChildAt(1);
        				txt.setText(Clock.getTimeString(items[marks[i][j]].getTimePlayed()));
        			}
        			else {
        				table.removeView(rw);
        			}
        		}
        		
        	}	
        	
        }	
        ScrollView screen=(ScrollView)findViewById(R.id.stats_screen);
        screen.setBackgroundDrawable(UIPreference.getBackgroundFactory().createBackground(this));
    }
    
    private TableRow caption(int row) {
    	int id=R.id.stats_session_caption_1;
    	switch(row) {
    	case 1:id=R.id.stats_session_caption_2; break;
    	case 2:id=R.id.stats_session_caption_3; break;
    	case 3:id=R.id.stats_session_caption_4; break;
    	}
    	return (TableRow)findViewById(id);
    }
    
    private TableRow header(int row) {
    	int id=R.id.stats_session_header_1;
    	switch(row) {
    	case 1:id=R.id.stats_session_header_2; break;
    	case 2:id=R.id.stats_session_header_3; break;
    	case 3:id=R.id.stats_session_header_4; break;
    	}
    	return (TableRow)findViewById(id);
    }
    
	private void replay(int i, int j) {
		try {
			Log.d("HRD", "Replay history for (" + i + "," + j + ")");
			Intent intent = new Intent(this, ReplayPanel.class);
			intent.putExtra(DBConstants.INDEX, items[marks[i][j]].getId());
			startActivity(intent);
		} catch (Exception ex) {
			Log.e("HRD", "Error in replaying history for (" + i + "," + j + ")", ex);
		}
	}
    
    private TableRow content(int type,int row) {
    	int id=-1;
    	switch(type) {
    	case 0:
    	switch(row) {
    	case 0:id=R.id.stats_session_data_1_1; break;
    	case 1:id=R.id.stats_session_data_1_2; break;
    	case 2:id=R.id.stats_session_data_1_3; break;
    	case 3:id=R.id.stats_session_data_1_4; break;
    	case 4:id=R.id.stats_session_data_1_5; break;
    	} break;
    	case 1:
        	switch(row) {
        	case 0:id=R.id.stats_session_data_2_1; break;
        	case 1:id=R.id.stats_session_data_2_2; break;
        	case 2:id=R.id.stats_session_data_2_3; break;
        	case 3:id=R.id.stats_session_data_2_4; break;
        	case 4:id=R.id.stats_session_data_2_5; break;
        	} break;
    	case 2:
        	switch(row) {
        	case 0:id=R.id.stats_session_data_3_1; break;
        	case 1:id=R.id.stats_session_data_3_2; break;
        	case 2:id=R.id.stats_session_data_3_3; break;
        	case 3:id=R.id.stats_session_data_3_4; break;
        	case 4:id=R.id.stats_session_data_3_5; break;
        	} break;        	
    	case 3:
        	switch(row) {
        	case 0:id=R.id.stats_session_data_4_1; break;
        	case 1:id=R.id.stats_session_data_4_2; break;
        	case 2:id=R.id.stats_session_data_4_3; break;
        	case 3:id=R.id.stats_session_data_4_4; break;
        	case 4:id=R.id.stats_session_data_4_5; break;
        	} break;        	
    	}
    	return (TableRow)findViewById(id);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playClassic1(View v) {
    	replay(0, 0);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playClassic2(View v) {
    	replay(0, 1);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playClassic3(View v) {
    	replay(0, 2);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void playClassic4(View v) {
    	replay(0, 3);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void playClassic5(View v) {
    	replay(0, 4);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playCorner1(View v) {
    	replay(1, 0);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playCorner2(View v) {
    	replay(1, 1);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playCorner3(View v) {
    	replay(1, 2);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void playCorner4(View v) {
    	replay(1, 3);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void playCorner5(View v) {
    	replay(1, 4);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playTwoMarshal1(View v) {
    	replay(2, 0);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playTwoMarshal2(View v) {
    	replay(2, 1);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playTwoMarshal3(View v) {
    	replay(2, 2);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void playTwoMarshal4(View v) {
    	replay(2, 3);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void playTwoMarshal5(View v) {
    	replay(2, 4);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playThreeMarshal1(View v) {
    	replay(3, 0);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playThreeMarshal2(View v) {
    	replay(3, 1);
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void playThreeMarshal3(View v) {
    	replay(3, 2);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void playThreeMarshal4(View v) {
    	replay(3, 3);
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void playThreeMarshal5(View v) {
    	replay(3, 4);
    }

}
