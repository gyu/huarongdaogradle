package com.robotsoft.android.huarongdao;

import android.net.Uri;

public class DBConstants {
	public static final String LENGTH="moves";
	public static final String TYPE="type";
	public static final String TIME="time";
	public static final String INDEX="step";
	public static final String MOVE="move";
	public static final String DATA="data";
	public static final String MUSIC="music";
	public static final String SEED="seed";
	public static final String LOOP="loop";
}
