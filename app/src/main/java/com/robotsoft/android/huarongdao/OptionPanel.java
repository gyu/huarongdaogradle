package com.robotsoft.android.huarongdao;

import com.robotsoft.android.base.BackgroundFactory;
import com.robotsoft.android.base.MenuPanel;
import com.robotsoft.android.base.UIPreference;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;

public class OptionPanel extends Activity {
	public static final int Shuffle=109;
	public static final int Type=110;
	public static final int Type1=111;
	public static final int Type2=112;
	public static final int Type3=113;
	public static final int Type4=114;
	public final static int Apply=115;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        setContentView(extras.getInt(UIPreference.Claz));
        TableLayout table=(TableLayout)findViewById(extras.getInt(UIPreference.Component));
        BackgroundFactory bf=UIPreference.getBackgroundFactory();
        table.setBackgroundDrawable(bf.createFrame(this));
	}
	
    @SuppressWarnings({"UnusedDeclaration"})
    public void doShuffle(View v) {
    	setResult(Shuffle);
        finish();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void doType(View v) {
    	setResult(Type);
        finish();
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void type1(View v) {
    	setResult(Type1);
        finish();
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void type2(View v) {
    	setResult(Type2);
        finish();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void type3(View v) {
    	setResult(Type3);
        finish();
    }

    @SuppressWarnings({"UnusedDeclaration"})
    public void type4(View v) {
    	setResult(Type4);
        finish();
    }

}
