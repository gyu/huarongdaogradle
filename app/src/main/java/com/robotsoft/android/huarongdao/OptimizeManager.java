package com.robotsoft.android.huarongdao;

import java.util.Vector;

public class OptimizeManager {
	private Vector[] histories;
	private Sample[] samples;
	private long[] bestPath;
	private int total;
	private long left;
	private long right;
	
	public OptimizeManager(Sample[] samples, Sample current, Vector[] histories) {
		super();
		left=current.getLeft();
		right=current.getRight();
		iterate(samples, current, histories);
	}

	private void doOptimize(Sample current) {
		while(true) {
			if((current.getLeft()==left&&current.getRight()==right)||
					(current.getLeft()==right&&current.getRight()==left)) {
				break;
			}
			if(current.getIndex()==total||SampleUtil.isDuplicated(current,histories)) {
				current=samples[current.getIndex()-1];
			}
			
			int index=current.getIndex();
			current=current.next();
			while(current==null) {
				index--;
			    if(index<0) return;
			    current=samples[index].next();
			}
			samples[current.getIndex()]=current;
		}
		iterate(samples,current, histories);
	}
	
	private void iterate(Sample[] samples, Sample current, Vector[] histories)  {
		total=current.getIndex()-1;
		this.histories=new Vector[total+1];
		this.samples=new Sample[total+1];
		System.arraycopy(samples, 0, this.samples, 0, total+1);
		System.arraycopy(histories, 0, this.histories, 0, total+1);
		bestPath=new long[total*2+2];
		for(int i=1,j=0; i<=total+1; i++,j++) {
			bestPath[j++]=samples[i].getLeft();
			bestPath[j]=samples[i].getRight();
		}
		if(total>1) doOptimize(samples[total]);
	}

	public long[] getBestPath() {
		return bestPath;
	}

}
