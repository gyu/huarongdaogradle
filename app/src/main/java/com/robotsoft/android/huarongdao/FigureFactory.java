package com.robotsoft.android.huarongdao;


import com.robotsoft.android.base.UIPreference;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;

public class FigureFactory {
	
	public static Figure[] initFigures(int type, int width, int height, Resources res) {
		long[] val=SampleUtil.getSamples(type);
		return initFigures(type,  width, height,  res, val[0], val[1]);
	}
	
	public static Figure[] initFigures(int type, int width, int height, Resources res,int rndSeed, int rndNumber) {
		long[] val=SampleUtil.getSamples(type);
		if(rndNumber>0&&rndSeed>0) {
		    val = new RandomWalk(rndSeed,rndNumber,val[0],val[1]).getValues();
		}
		return initFigures(type,  width, height,res, val[0], val[1]);
	}
	
	private static Figure[] initFigures(int type, int width, int height, Resources res,long val1,long val2) {
		BitmapFactory.Options options = new BitmapFactory.Options();
        options.inTargetDensity = 1;
        options.inDensity = 1;
        int border=UIPreference.getBorderSpan();
        int d=Math.min((width-2*border)/4,(height-2*border)/5);
        int k=d<64?0:d>84?2:1;
		Bitmap[] bmps=new Bitmap[10];
		for(int i=0; i<10; i++) {
			int resId;
			switch(i) {
			case 0:resId=k==1?R.drawable.caocao:k==0?R.drawable.l_caocao:R.drawable.h_caocao; break;
			case 1:resId=k==1?R.drawable.guanyu2:k==0?R.drawable.l_guanyu2:R.drawable.h_guanyu2; break;
			case 2:resId=type<2?(k==1?R.drawable.zhangfei1:k==0?R.drawable.l_zhangfei1:R.drawable.h_zhangfei1):
				k==1?R.drawable.zhangfei2:k==0?R.drawable.l_zhangfei2:R.drawable.h_zhangfei2; break;
			case 3:resId=type<3?(k==1?R.drawable.zhaoyun1:k==0?R.drawable.l_zhaoyun1:R.drawable.h_zhaoyun1):k==1?R.drawable.zhaoyun2:k==0?R.drawable.l_zhaoyun2:R.drawable.h_zhaoyun2; break;
			case 4:resId=k==1?R.drawable.machao1:k==0?R.drawable.l_machao1:R.drawable.h_machao1; break;
			case 5:resId=k==1?R.drawable.huangzhong1:k==0?R.drawable.l_huangzhong1:R.drawable.h_huangzhong1; break;
			case 6:resId=k==1?R.drawable.bing1:k==0?R.drawable.l_bing1:R.drawable.h_bing1; break;
			case 7:resId=k==1?R.drawable.bing2:k==0?R.drawable.l_bing2:R.drawable.h_bing2; break;
			case 8:resId=k==1?R.drawable.bing3:k==0?R.drawable.l_bing3:R.drawable.h_bing3; break;
			default:resId=k==1?R.drawable.bing4:k==0?R.drawable.l_bing4:R.drawable.h_bing4; break;
			}
			bmps[i]=BitmapFactory.decodeResource(res, resId);
		}
		
		
		int dim=bmps[9].getWidth()+4;
		int ox=(width-4*dim)/2;
		int oy=(height>5*dim+2*(border+2))?border+2:height/2-5*dim/2;
		Figure[] fs=new Figure[10];
		int[] data = SampleUtil.getData(val1, val2);
		int j1=1;
		int j3=6;
		int[][] rest=new int[4][];
		int j2=0;
		for (int i = 0; i < 20; i++) {
			int tp = data[i]-1;
			if (tp<0 || tp == 4 || (tp < 2 && i >= 15)) {
				continue;
			}
			int[] xy = Algo.Bounds[tp][i];
			if(tp==0) {
				fs[0]=new Figure(0,xy[0],xy[1],ox, oy, dim, bmps[0]);
			}
			else if(tp==3) {
				fs[j3]=new Figure(j3, xy[0],xy[1],ox, oy, dim, bmps[j3]);
				j3++;
			}
			else if(tp==1) {
				fs[j1]=new Figure(j1,xy[0],xy[1],ox, oy, dim,bmps[j1]);
				j1++;	
			}
			else {
				rest[j2++]=xy;
			}
		}
		for(int i=0,j=6-j2; i<j2; i++,j++) {
			fs[j]=new Figure(j,rest[i][0],rest[i][1],ox, oy, dim, bmps[j]);
		}
		return fs;
	}
	
	
	public static Figure[] createFigures(Database db, Resources res, int width,int height) {
		GameData gd=(GameData)db.getGameData();
		int type = gd.getFormat();
		Figure[] fs = initFigures(type, width, height, res, gd.getSeed(), gd.getLoop());
		int n = gd.getMoves().size();
		Log.d("HRD","Moved steps: "+n);
		int dx;
		int dy;
		int p=-1;
		for(int i=0; i<n; i++) {
			int k=gd.getMoves().get(i);
			dx=0;
			dy=0;
			switch(k/10) {
			case 0:dy=-1;break;
			case 1:dx=-1;break;
			case 2:dx=1;break;
			case 3:dy=1;break;
			}
			p=k%10;
			fs[p].move(dx, dy);
		 }
		 fs[p==-1?0:p].setSelected(true);
		 return fs;
	}
	
	public static int[][] mapOf(Figure[] figs) {
		int[][] map = new int[5][4];
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 4; j++) {
				for (int k = 0; k < 10; k++) {
					if (figs[k].in(j, i)) {
						map[i][j] = k + 1;
						break;
					}
				}
			}
		}
		return map;
	}

}
