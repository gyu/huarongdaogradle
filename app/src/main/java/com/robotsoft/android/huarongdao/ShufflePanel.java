package com.robotsoft.android.huarongdao;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import com.robotsoft.android.base.BackgroundFactory;
import com.robotsoft.android.base.ListViewAdapter;
import com.robotsoft.android.base.UIPreference;

public class ShufflePanel extends Activity {
	public final static String Seed="seed";
	public final static String Number="number";
	private Spinner spinner;
	private EditText seed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shuffle_panel);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Bundle extras = getIntent().getExtras();
        //int selected=extras.getInt(DBConstants.TYPE, 0);
        
        TableLayout view=(TableLayout) findViewById(R.id.shuffle_panel);
        view.setBackgroundDrawable(UIPreference.getBackgroundFactory().createFrame(this));
        spinner = (Spinner) findViewById(R.id.number_value);
        seed=(EditText)findViewById(R.id.seed_value);
        int[] is=getResources().getIntArray(R.array.numbers);
        Integer[] intVals=new Integer[is.length];
        for(int i=0; i<is.length; i++)intVals[i]=is[i];
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this,
        		R.layout.simple_spinner_item,intVals);
        spinner.setAdapter(adapter);
        
       /*ListView list = (ListView) findViewById(R.id.option_list);
        spinner = (Spinner) findViewById(R.id.number_value);
        seed=(EditText)findViewById(R.id.seed_value);
        spinner.setBackgroundDrawable(new SpinnerDrawable(spinner.getBackground()));
        int[] is=getResources().getIntArray(R.array.numbers);
        Integer[] intVals=new Integer[is.length];
        for(int i=0; i<is.length; i++)intVals[i]=is[i];
        ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(this,R.layout.simple_spinner_item,intVals);
        spinner.setAdapter(adapter);
        String[] vals=getResources().getStringArray(R.array.types);
        listAdapter = new ListViewAdapter<String>(this, list, R.layout.list_item, vals, selected);
        list.setSelector(R.drawable.list_selector);*/
    }
    
    @SuppressWarnings({"UnusedDeclaration"})
    public void apply(View view) {
    	Intent intent=new Intent();
    	Editable e=seed.getText();
    	String v=(e==null)?null:e.toString().trim();
    	if(v!=null&&!v.equals("")) {
    		try {
    			intent.putExtra(Seed, Integer.parseInt(v));
    			int k=spinner.getSelectedItemPosition();
    	        int[] is=getResources().getIntArray(R.array.numbers);
    			intent.putExtra(Number, is[k]);
    		}
    		catch(NumberFormatException ex) {
    			Log.e("HRD","number format error ",ex);
    		}
    	}
    	Log.d("HRD","number format");
    	setResult(OptionPanel.Apply, intent);
    	finish();
    }
    
}



