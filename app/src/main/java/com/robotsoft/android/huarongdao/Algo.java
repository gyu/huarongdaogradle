package com.robotsoft.android.huarongdao;

public class Algo {
	public static int[][][][] Vacancies=new int[4][20][4][];
	public static int[][][][][] Results=new int[4][20][4][][];
	public static int[][][] Bounds=new int[4][20][];
	public static int[][] Dims={{2,2},{2,1},{1,2},{1,1}};
	static {
		determineBounds();
		determineVacancies();
		determineResults();
	}
	
	public static void determineBounds() {
		for(int i=0; i<20; i++) {
			Bounds[0][i]=determineBounds22(i);
			Bounds[1][i]=determineBounds21(i);
			Bounds[2][i]=determineBounds12(i);
			Bounds[3][i]=determineBounds11(i);
		}
	}
	
	public static void determineVacancies() {
		for(int i=0; i<20; i++) {
			Vacancies[0][i]=determineVacancies22(i);
			Vacancies[1][i]=determineVacancies21(i);
			Vacancies[2][i]=determineVacancies12(i);
			Vacancies[3][i]=determineVacancies11(i);
		}
	}
	
	public static void determineResults() {
		for(int i=0; i<20; i++) {
			for(int j=0; j<4; j++) {
				Results[0][i][j]=determineResult22(i,j);
				Results[1][i][j]=determineResult21(i,j);
				Results[2][i][j]=determineResult12(i,j);
				Results[3][i][j]=determineResult11(i,j);
			}
		}
	}
	
	private static int[] determineBounds22(int pos) {
		int y = pos%5;
		if(y==4) return new int[0];
		int c= pos/5;
		int[] w = new int[2];
		w[1] = y;
		w[0] = (c==0)?0:c==2?2:1;
		return w;
	}
	
	private static int[] determineBounds21(int pos) {
		int y=pos%5;
		int c=pos/5;
		int[] w=new int[2];
		w[1]=y;
		w[0]=(c==0)?0:c==2?2:1;
		return w;
	}
	
	private static int[] determineBounds12(int pos) {
		int y=pos%5;
		if(y==4) return new int[0];
		int[] w=new int[2];
		int c=pos/5;
		w[0]=(c<2)?c:5-c;
		w[1]=y;
		return w;
	}
	
	private static int[] determineBounds11(int pos) {
		int[] w=new int[2];
		int c=pos/5;
		w[0]=(c<2)?c:5-c;
		w[1]=pos%5;
		return w;
	}
	
	private static int[][] determineVacancies22(int pos) {
		int[] xy = Bounds[0][pos];
		if(xy.length ==0) return new int[4][0];
		int[][] q = new int[4][0];
		if(xy[1]>0) {
			q[0] = new int[]{toIndex(xy[0],xy[1]-1),toIndex(xy[0]+1,xy[1]-1)};
		}
		if(xy[0]>0) {
			q[1] = new int[]{toIndex(xy[0]-1,xy[1]),toIndex(xy[0]-1,xy[1]+1)};
		}
		if(xy[0]<2) {
			q[2] = new int[]{toIndex(xy[0]+2,xy[1]),toIndex(xy[0]+2,xy[1]+1)};
		}
		if(xy[1]<3) {
			q[3] = new int[]{toIndex(xy[0],xy[1]+2),toIndex(xy[0]+1,xy[1]+2)};
		}
		return q;
	}
	
	
	private static int[][] determineVacancies21(int pos) {
		int[] xy = Bounds[1][pos];
		if(xy.length==0) return new int[4][0];
		int[][] q = new int[4][0];
		if(xy[1]>0) {
			q[0]=new int[]{toIndex(xy[0],xy[1]-1),toIndex(xy[0]+1,xy[1]-1)};
		}
		if(xy[0]>0) {
			q[1]=new int[]{toIndex(xy[0]-1,xy[1])};
		}
		if(xy[0]<2) {
			q[2]=new int[]{toIndex(xy[0]+2,xy[1])};
		}
		if(xy[1]<4) {
			q[3]=new int[]{toIndex(xy[0],xy[1]+1),toIndex(xy[0]+1,xy[1]+1)};
		}
		return q;
	}
	
	private static int[][] determineVacancies12(int pos) {
		int[] xy = Bounds[2][pos];
		if(xy.length==0) return new int[4][0];
		int[][] q = new int[4][0];
		if(xy[1]>0) {
			q[0] = new int[]{toIndex(xy[0],xy[1]-1)};
		}
		if(xy[0]>0) {
			q[1] = new int[]{toIndex(xy[0]-1,xy[1]),toIndex(xy[0]-1,xy[1]+1)};
		}
		if(xy[0]<3) {
			q[2]=new int[]{toIndex(xy[0]+1,xy[1]),toIndex(xy[0]+1,xy[1]+1)};
		}
		if(xy[1]<3) {
			q[3]=new int[]{toIndex(xy[0],xy[1]+2)};
		}
		return q;
	}
	
	private static int[][] determineVacancies11(int pos) {
		int[] xy=Bounds[3][pos];
		if(xy.length==0) return new int[4][0];
		int[][] q = new int[4][0];
		if(xy[1]>0) {
			q[0] = new int[]{toIndex(xy[0],xy[1]-1)};
		}
		if(xy[0]>0) {
			q[1] = new int[]{toIndex(xy[0]-1,xy[1])};
		}
		if(xy[0]<3) {
			q[2]=new int[]{toIndex(xy[0]+1,xy[1])};
		}
		if(xy[1]<4) {
			q[3]=new int[]{toIndex(xy[0],xy[1]+1)};
		}
		return q;
	}

	
	private static int[][] determineResult22(int pos, int dir) {
		int[] xy=Vacancies[0][pos][dir];
		int n=xy.length;
		if(n==0) return new int[0][0];
		int[][] q=new int[6][2];
		int c = xy[0]/5;
		int x = (c<2)?c:5-c;
		if(dir==0) {
			q[0][0]=xy[0];  q[1][0]=xy[1];
			q[2][0]=xy[0]+1; q[3][0]=xy[1]+1;
			q[4][0]=xy[0]+2; q[5][0]=xy[1]+2;
		}
		else if(dir==3) {
			q[0][0]=xy[0]-1; q[1][0]=xy[1]-1;
			q[2][0]=xy[0];  q[3][0]=xy[1];
			q[4][0]=xy[0]-2; q[5][0]=xy[1]-2;
		}
		else {
			int y=xy[0]%5;
			if(dir==1) {
			   q[0][0]=xy[0]; q[2][0]=q[0][0]+1; 
			   q[1][0]=toIndex(x+1,y); q[3][0]=q[1][0]+1; 
			   q[4][0]=toIndex(x+2,y); q[5][0]=q[4][0]+1;
		   }
			else {
				q[0][0]=toIndex(x-1,y);  q[2][0]=q[0][0]+1;
				q[1][0]=xy[0]; q[3][0]=q[1][0]+1; 
				q[4][0]=toIndex(x-2,y); q[5][0]=q[4][0]+1;
				x--;
			}
		}
		if(x==2) {
			q[1][1]=1;
		}
		else {
			q[0][1]=1;
		    if(x==1) {
			    q[1][1]=1;
		    }
		}
		q[4][1]=5;
		q[5][1]=5;
		return q;
	}

	private static int[][] determineResult21(int pos, int dir) {
		int[] xy=Vacancies[1][pos][dir];
		int n=xy.length;
		if(n==0) return new int[0][0];
		int[][] q;
		int c=xy[0]/5;
		int x=(c<2)?c:5-c;
		if (dir == 0 || dir == 3) {
			q = new int[4][2];
			if (dir == 0) {
				q[0][0] = xy[0];
				q[1][0] = xy[1];
				q[2][0] = xy[0] + 1;
				q[3][0] = xy[1] + 1;
			} else {
				q[0][0] = xy[0];
				q[1][0] = xy[1];
				q[2][0] = xy[0] - 1;
				q[3][0] = xy[1] - 1;
			}
			if(x==2) {
				q[1][1]=2;
			}
			else {
			   q[0][1] = 2;
			   if(x==1) q[1][1]=2;
			}
			q[2][1] = 5;
			q[3][1] = 5;
		}		
		else {
			q=new int[3][2];
			int y=xy[0]%5;
			if(dir==1) {
			   q[0][0]=xy[0]; q[1][0]=toIndex(x+1,y); q[2][0]=toIndex(x+2,y);
		   }
			else {
			   q[0][0]=toIndex(x-1,y); q[1][0]=xy[0]; q[2][0]=toIndex(x-2,y);
			   x--;
			}
			if(x==2) {
				q[1][1] = 2;
			}
			else {
			    q[0][1] = 2;
			    if(x==1) q[1][1]=2;
			}
			q[2][1] = 5;
		}
		return q;
	}

	private static int[][] determineResult12(int pos, int dir) {
		int[] xy=Vacancies[2][pos][dir];
		int n=xy.length;
		if(n==0) return new int[0][0];
		int[][] q;
		if (dir == 0 || dir == 3) {
			q = new int[3][2];
			if (dir == 0) {
				q[0][0] = xy[0];
				q[1][0] = xy[0] + 1;
				q[2][0] = xy[0] + 2;
			} else {
				q[0][0] = xy[0] - 1;
				q[1][0] = xy[0];
				q[2][0] = xy[0] - 2;
			}
			q[0][1] = 3;
			q[2][1] = 5;
		} else {
			q = new int[4][2];
			int y = xy[0] % 5;
			int c = xy[0] / 5;
			int x = (c < 2) ? c : 5 - c;
			if (dir == 1) {
				q[0][0] = xy[0]; q[1][0] = xy[0] + 1;
				q[2][0] = toIndex(x + 1, y); q[3][0] = q[2][0] + 1;
			} else {
				q[0][0] = xy[0]; q[1][0] = xy[0] + 1;
				q[2][0] = toIndex(x - 1, y); q[3][0] = q[2][0] + 1;
			}
			q[0][1] = 3;
			q[2][1] = 5;
			q[3][1] = 5;
		}
		return q;
	}

	
	private static int[][] determineResult11(int pos, int dir) {
		int[] xy=Vacancies[3][pos][dir];
		int n=xy.length;
		if(n==0) return new int[0][0];
		int[][] q=new int[2][2];
		q[0][0]=xy[0];
        if(dir==0||dir==3) {
			q[1][0]=(dir==0)?xy[0]+1:xy[0]-1;
		}
		else  {
			int y=xy[0]%5;
			int c=xy[0]/5;
			int x=(c<2)?c:5-c;
			q[1][0]=(dir==1)?toIndex(x+1,y):toIndex(x-1,y);
		}
		q[0][1]=4;
		q[1][1]=5;
		return q;
	}
	
	private static int toIndex(int x,int y) {
		switch(x) {
		case 0:return y;
		case 1:return 5+y;
		case 3:return 10+y;
		default:return 15+y;
		}
	}

}
