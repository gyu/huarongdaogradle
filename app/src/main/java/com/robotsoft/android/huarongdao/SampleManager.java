package com.robotsoft.android.huarongdao;

import java.util.Vector;

public class SampleManager {
	private int total;
	private long[] targets;
	private Sample[] samples;
	private Vector[] histories;
	private Sample current;
	
	public  SampleManager(long l1,long l2, long[] targets, int step) {
		this.total=step;
		this.targets=targets;
		current=new Sample(l1,l2,0);
	}
	
	public long[] getSolution() {
		int n=targets.length;
		for(int i=0; i<n-2; i++ ) {
		   long m1=targets[i++];	
		   long m2=targets[i];
		   if(m1==current.getLeft()&&m2==current.getRight()) {
			   return new long[]{targets[i+1],targets[i+2]};
		   }
		   if(m2==current.getLeft()&&m1==current.getRight()) {
			   return new long[]{targets[i+2],targets[i+1]};
		   }
		}
		samples=new Sample[total+1];
		samples[0]=current;
		histories=new Vector[total];
		for(int i=0; i<total; i++) {
			histories[i]=new Vector();	
		}
		while(true) {
			if(SampleUtil.isDuplicated(current,histories)) {
				current=samples[current.getIndex()-1];
			}
			else if(isSolved(current)) {
				break;
			}
			else if(current.getIndex()==total) {
				current=samples[current.getIndex()-1];
			}
			int inx=current.getIndex();
			current=current.next();
			while(current==null) {
				inx--;
			    if(inx<0) return null;
			    current=samples[inx].next();
			}
			samples[current.getIndex()]=current;
		}
		OptimizeManager om=new OptimizeManager(samples,current, histories);
		return om.getBestPath();
	}

	private boolean isSolved(Sample sample) {
	      return solved(sample)>=0;
	}

	private int solved(Sample sample) {
		long l1=sample.getLeft();
		long l2=sample.getRight();
		for(int i=0,n=targets.length; i<n; i++) {
			long m1=targets[i++];
			long m2=targets[i];
			if((l1==m1&&l2==m2)||(l1==m2&&l2==m1)) {
				return i;
			}	
		}
		return -1;
	}
}
