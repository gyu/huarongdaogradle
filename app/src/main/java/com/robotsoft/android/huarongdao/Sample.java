package com.robotsoft.android.huarongdao;

import java.util.Vector;

public class Sample {
	private long left;
	private long right;
	private int index;
	private int[][] routines;
	private int count;

	public Sample(long left, long right, int index) {
		super();
		this.left = left;
		this.right = right;
		this.index = index;
		count = 0;
	}

	public Sample next() {
		if (routines == null) {
			routines = findRoutines();
		}
		if (count == routines.length) {
			return null;
		}
		return create(left, right, index, routines[count++]);
	}

	private Sample create(long l1, long l2, int inx, int[] params) {
		int[] data=SampleUtil.getData(left, right);
		int[][] chgs = Algo.Results[params[1]][params[0]][params[2]];
		for (int i = 0; i < chgs.length; i++) {
			data[chgs[i][0]] = chgs[i][1];
		}
		long[] ll= SampleUtil.getPattern(data);
		return new Sample(ll[0],ll[1],inx+1);
	}

	private int[][] findRoutines() {
		int[] data=SampleUtil.getData(left, right);
		Vector l = new Vector();
		for (int i = 0; i < 20; i++) {
			int j = data[i];
			if (j == 0 || j == 5)
				continue;
			if (i >= 15 && j < 3)
				continue;
			for (int k = 0; k < 4; k++) {
				int[] q = Algo.Vacancies[j - 1][i][k];
				if (q.length > 0) {
					if (data[q[0]] == 5) {
						if (q.length == 1 || data[q[1]] == 5) {
							l.addElement(new int[] { i, j - 1, k });
						}
					}
				}
			}
		}
		int n=l.size();
		int[][] res=new int[n][];
		for(int i=0; i<n; i++) {
			res[i]=(int[])l.elementAt(i);
		}
		return res;
	}

	public int getIndex() {
		return index;
	}

	public long getLeft() {
		return left;
	}

	public long getRight() {
		return right;
	}

}
