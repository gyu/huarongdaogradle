package com.robotsoft.android.huarongdao;

import com.robotsoft.android.base.UIPreference;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

public class Figure {
	private int dim;
	private int ox;
	private int oy;
	private Bitmap image;
	private int x;
	private int y;
	private boolean selected;
	private boolean marked;
	private int width;
	private int height;
	private int index;
	private int type;

	public Figure(int index, int x, int y, int ox, int oy, int dim, Bitmap image) {
		this.dim = dim;
		this.ox = ox;
		this.oy = oy;
		this.x = x;
		this.y = y;
		this.index = index;
		this.image = image;
		width = (image.getWidth() + 4) / dim;
		height = (image.getHeight() + 4) / dim;
		if (width == 2)
			type = height == 2 ? 0 : 1;
		else
			type = height == 2 ? 2 : 3;
	}

	public void paint(Resources resources, Canvas g) {
		if (image != null) {
			paint(resources, g, x * dim + ox+2,y * dim + oy+2,selected,marked);
		}
	}

	public void paint(Resources resources, Canvas g, float[] xy) {
		paint(resources, g, (int)((x+xy[0]) * dim+0.5) + ox+2, (int)((y+xy[1]) * dim+0.5) + oy+2, true, true);
	}
	
	private void paint(Resources resources,Canvas g, int x, int y, boolean sel, boolean mkd) {
		
		g.drawBitmap(image, x, y, null);
		if (sel) {
			Paint p = new Paint();
			p.setColor(resources.getColor(mkd ? R.color.mark: R.color.highlight));
			p.setStyle(Paint.Style.STROKE);
			p.setStrokeWidth(mkd ? 3 : 2);
			g.drawRect(x - 1, y - 1, x + image.getWidth() + 1, y + image.getHeight() + 1, p);
		}
	}

	public int getIndex() {
		return index;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		if (!selected) {
			marked = false;
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public boolean in(int xx, int yy) {
		return (xx >= x && xx < x + width && yy >= y && yy < y + height);
	}

	public boolean contains(Resources res, int xx, int yy, int level) {
		int w = image.getWidth();
		int h = image.getHeight();
		int a = x * dim + ox;
		int b = y * dim + oy;
		if(res==null||level==0) {
			return (xx >= a && xx < a + w && yy >= b && yy < b + h);
		}
		int d=UIPreference.getGestureThreshold(res)*level;
		return (xx >= a-d && xx < a + w+d && yy >= b-d && yy < b + h+d);
	}

	public void move(int dx, int dy) {
		x += dx;
		y += dy;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public boolean isSelected() {
		return selected;
	}

	public int getType() {
		return type;
	}

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
		if (marked)
			selected = true;
	}

	public void setXy(int i, int j) {
		x=i;
		y=j;
	}

	public int getDim() {
		return dim;
	}

}
