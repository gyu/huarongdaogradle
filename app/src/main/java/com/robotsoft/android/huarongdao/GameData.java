package com.robotsoft.android.huarongdao;

import java.util.ArrayList;
import java.util.List;

import com.robotsoft.android.base.AbstractGameData;

public class GameData extends AbstractGameData {
	private int seed;
	private int loop;
	
	private List<Integer> moves;
	
	
	public GameData() {
	}

	public GameData(int format) {
		setFormat(format);
		moves=new ArrayList<Integer>();
	}

	public List<Integer> getMoves() {
		return moves;
	}
	
	public void newMove(int time,int move) {
		moves.add(move);
		setTimePlayed(time);
	}

	public void setMoves(List<Integer> moves) {
		this.moves = moves;
	}

	public int totalMoves() {
		return moves.size();
	}

	public int getSeed() {
		return seed;
	}

	public void setSeed(int seed) {
		this.seed = seed;
	}

	public int getLoop() {
		return loop;
	}

	public void setLoop(int loop) {
		this.loop = loop;
	}
	
}
