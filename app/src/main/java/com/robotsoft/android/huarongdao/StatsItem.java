package com.robotsoft.android.huarongdao;

import java.util.List;

import com.robotsoft.android.base.AbstractStatsItem;

public class StatsItem extends AbstractStatsItem {
	private int type;
	private byte[] moves;
	
	public StatsItem(int type, int time, List<Integer> moves) {
		this(-1, type,time,moves);
	}

	
	public StatsItem(int type, int time, byte[] moves) {
		this(-1, type,time,moves);
	}
	
	public StatsItem(int id, int type, int time, byte[] moves) {
		super(id,time);
		this.type = type;
		this.moves = moves;
	}
	
	public StatsItem(int id, int type, int time, List<Integer> moves) {
		super(id,time);
		this.type = type;
		int n=moves.size();
		this.moves=new byte[n];
		for(int i=0; i<n; i++)this.moves[i] = moves.get(i).byteValue();
	}


	public int getType() {
		return type;
	}

	public byte[] getMoves() {
		return moves;
	}

}
