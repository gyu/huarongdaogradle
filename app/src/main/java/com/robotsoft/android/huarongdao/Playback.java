package com.robotsoft.android.huarongdao;

import com.robotsoft.android.huarongdao.ReplayPanel.ProgressListener;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class Playback extends Animation {
	private final static int Total = 5;
	private final static float Frac = 0.2f;
	private int count;
	private int dx;
	private int dy;
	private Figure figure;
	private ProgressListener listener;
	private View view;

	public Playback(View view, Figure f, int dx, int dy,
			ProgressListener listener) {
		this.view=view;
		count = 0;
		this.dx = dx;
		this.dy = dy;
		this.figure = f;
		f.setMarked(true);
		this.listener = listener;
		setDuration(100);
		setRepeatCount(Total);
	}

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		if (count< Total) {
			count++;
		}
		if(count==Total) {
			try {
				view.clearAnimation();
				if(figure!=null) {
					figure.move(-dx, -dy);
					figure = null;
					listener.onBack();
				}
			} catch (Throwable ex) {
				Log.e("HRD", "Transformation Error", ex);
			}
		}
	}

	public float[] getPosition() {
		float c = count * Frac;
		return new float[] { -dx * c, -dy * c };
	}

	public Figure getFigure() {
		return figure;
	}

}
