package com.robotsoft.android.huarongdao;

import static android.provider.BaseColumns._ID;

import java.util.ArrayList;
import java.util.List;

import com.robotsoft.android.base.AbstractDBHelper;
import com.robotsoft.android.base.AbstractDatabase;
import com.robotsoft.android.base.AbstractGameData;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import static com.robotsoft.android.base.AbstractDBHelper.FIGURE_TABLE_NAME;
import static com.robotsoft.android.base.AbstractDBHelper.GAME_TABLE_NAME;
import static com.robotsoft.android.huarongdao.DBConstants.*;
import static com.robotsoft.android.base.AbstractDBHelper.*;


public class Database extends AbstractDatabase{
	public Database(Context context) {
		super(context);
	}
	
	public int undo() {
		GameData gd=(GameData)getGameData();
		List<Integer> lst=gd.getMoves();
		int n=lst.size();
		int move=lst.remove(n-1);
		helper.getWritableDatabase().delete(FIGURE_TABLE_NAME, INDEX+"="+n, null);
		return move;
	}
	
	private List<Integer> getMoves() {
		List<Integer> list=new ArrayList<Integer>();
		Cursor cursor=null;
		try {
		cursor = helper.getReadableDatabase().query(FIGURE_TABLE_NAME, 
				new String[]{INDEX, MOVE}, null, null, null,null,INDEX);
		if (cursor.moveToFirst()) {
			do {
				list.add(cursor.getInt(1));
	    	}while(cursor.moveToNext());
        }
		}catch(Exception ex) {
		}
		finally {
			if(cursor!=null) {
				cursor.close();
			}
		}
		return list;
	}

	
	protected AbstractGameData loadGameData() {
		GameData gd=new GameData();
		SQLiteDatabase db=helper.getReadableDatabase();
		Cursor cursor =null;
		try {
		cursor = db.query(GAME_TABLE_NAME, 
				new String[]{TYPE,TIME,SEED,LOOP,MUSIC}, null, null, null,null,null);
		if (cursor.moveToFirst()) {
			gd.setFormat(cursor.getInt(0));
			gd.setTimePlayed(cursor.getInt(1));
			gd.setSeed(cursor.getInt(2));
			gd.setLoop(cursor.getInt(3));
			gd.setPlayMusic(cursor.getInt(4)>0);
        } 
		
		}catch(Exception ex) {
		}
		finally {
			if(cursor!=null) {
				cursor.close();
			}
			
		}
		gd.setMoves(getMoves());
		return gd;
	}
	
	public StatsItem getStatsItem(int id) {
		StatsItem val=null;
		Cursor cursor =helper.getReadableDatabase().query(STATS_TABLE_NAME, new String[]{_ID,TYPE,TIME,DATA}, _ID+"="+id, null, null,null,null);
    	if (cursor != null) {
    	   if (cursor.moveToFirst()) {
    		   val=new StatsItem(cursor.getInt(0),cursor.getInt(1),cursor.getInt(2),cursor.getBlob(3));
    	   }
           cursor.close();
        }
    	return val;
	}
	
    public StatsItem[] getStats(int type) {
    	
    	Cursor cursor = helper.getReadableDatabase().query(STATS_TABLE_NAME, new String[]{_ID,TYPE,TIME,DATA}, TYPE+"="+type, null, null,null, LENGTH+","+TIME);
    	if (cursor == null) {
            return null;
        } else if (! cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
    	StatsItem[] vals = new StatsItem[5];
    	int n = 0;
    	do {
    		vals[n++]=new StatsItem(cursor.getInt(0),cursor.getInt(1),cursor.getInt(2),cursor.getBlob(3));
    	} while(cursor.moveToNext());
    	cursor.close();
    	if(n==5) return vals;
    	StatsItem[] res = new StatsItem[n];
    	System.arraycopy(vals, 0, res, 0, n);
    	return res;
    }

	
    public StatsItem[] getStats() {
    	Cursor cursor = helper.getReadableDatabase().query(STATS_TABLE_NAME, new String[]{_ID,TYPE,TIME,DATA}, null, null, null,null,TYPE+","+LENGTH+","+TIME);
    	if (cursor == null) {
    		Log.d("HRD","No Stats");
            return null;
        } else if (! cursor.moveToFirst()) {
        	Log.d("HRD","Stats: 0");
            cursor.close();
            return null;
        }
    	StatsItem[] vals = new StatsItem[20];
    	int n = 0;
    	do {
    		vals[n++]=new StatsItem(cursor.getInt(0),cursor.getInt(1),cursor.getInt(2),cursor.getBlob(3));
    		Log.d("HRD","Stats: "+vals[n-1].getId()+","+vals[n-1].getTimePlayed()+","+vals[n-1].getMoves().length);
    	} while(cursor.moveToNext());
    	cursor.close();
    	if(n==20) return vals;
    	StatsItem[] res = new StatsItem[n];
    	System.arraycopy(vals, 0, res, 0, n);
    	return res;
    }

	public void newMove( int index, int dx, int dy, int time ) {
		int move=((dy==-1)?0:dx==-1?10:dx==1?20:30)+index;
		GameData gd=(GameData)game;
		gd.newMove(time, move);
		int count=gd.getMoves().size();
		insertMove(time, count, move);
	}
	
	public GameData typeChanged(int type) {
		SQLiteDatabase db = helper.getWritableDatabase();
		db.delete(FIGURE_TABLE_NAME, null, null);
		ContentValues values = new ContentValues();
	    values.put(TYPE, type);
	    values.put(TIME, 0);
	    values.put(SEED, -1);
	    values.put(LOOP, -1);
        db.update(GAME_TABLE_NAME, values,null,null);
        GameData gd=new GameData();
        game=gd;
        gd.setFormat(type);
        gd.setSeed(-1);
        gd.setLoop(-1);
        gd.setMoves(new ArrayList<Integer>());
        return gd;
	}
	
	public void doShuffle(int seed,int number) {
		SQLiteDatabase db = helper.getWritableDatabase();
		db.delete(FIGURE_TABLE_NAME, null, null);
		ContentValues values = new ContentValues();
	    values.put(TIME, 0);
	    values.put(SEED, seed);
	    values.put(LOOP, number);
        db.update(GAME_TABLE_NAME, values,null,null);
        game=null;
	}
	
	public void gameDone() {
		GameData gd=(GameData)game;
		int t = gd.getFormat();
		Log.d("HRD","Game done type "+t+","+gd.getTimePlayed()+","+gd.getMoves().size());
        StatsItem[] vals = getStats(t);
        int n = (vals==null)?0:vals.length;
        if(n>=5) {
        	int n1 = gd.getMoves().size();
        	int n2 = vals[4].getMoves().length;
        	if(n1>n2||(n1==n2&&gd.getTimePlayed()>=vals[4].getTimePlayed())) {
        	   	return;
        	}
   	    	deleteStats(vals[4].getId());
        }
        insertStats(new StatsItem(t,gd.getTimePlayed(),gd.getMoves()));
	}

	
	private void insertStats(StatsItem item) {
		SQLiteDatabase db = helper.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    values.put(TIME, item.getTimePlayed());
	    values.put(TYPE, item.getType());
	    values.put(LENGTH, item.getMoves().length);
	    values.put(DATA,item.getMoves());
        db.insertOrThrow(STATS_TABLE_NAME, null, values);
	}
	
	private void insertMove(int time, int index, int move) {
		SQLiteDatabase db = helper.getWritableDatabase();
	    ContentValues values = new ContentValues();
	    values.put(TIME, time);
	    db.update(GAME_TABLE_NAME, values,null,null);
	    values = new ContentValues();
	    values.put(INDEX, index);
	    values.put(MOVE, move);
	    db.insertOrThrow(FIGURE_TABLE_NAME, null, values);
	}
	
	private void deleteStats(int i) {
		helper.getWritableDatabase().delete(STATS_TABLE_NAME, _ID+"="+i, null);
	}

	@Override
	protected AbstractDBHelper create(Context context) {
		return new DBHelper(context);
	}


	@Override
	public void toggleMusic() {
		AbstractGameData gd=getGameData();
		boolean b=gd.isPlayMusic();
		gd.setPlayMusic(!b);
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(MUSIC, b?0:1);
		db.update(GAME_TABLE_NAME, values, null, null);
	}

}
