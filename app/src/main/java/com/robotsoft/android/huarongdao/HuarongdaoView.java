package com.robotsoft.android.huarongdao;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;

import com.robotsoft.android.base.AbstractDatabase;
import com.robotsoft.android.base.AbstractView;
import com.robotsoft.android.base.UIPreference;


public class HuarongdaoView extends AbstractView {
	private Figure[] figures;
	private long[] hints;
	private Fly fly;
	
	
	public HuarongdaoView(Context context) {
		super(context);
	}

	protected void doInit() {
		database.reload();
		figures = FigureFactory.createFigures((Database)database, getResources(), width, height);
	}

	@Override
	protected void doDraw(Canvas canvas) {
		if (figures != null) {
			int k=-1;
			for (int i = 0; i < figures.length; i++) {
				if (figures[i] != null) {
					if(fly!=null&&figures[i].isSelected())k=i;
					else figures[i].paint(getResources(),canvas);
				}
			}
			if(k!=-1) {
				figures[k].paint(getResources(),canvas, fly.getPosition());
			}
		}
	}
	
	public void undo() {
		int k = ((Database)database).undo();
		if (k < 0) return;
		Figure f =getSelected();
		if(f!=null)f.setSelected(false);
		int selected = k % 10;
		f = figures[selected];
		f.setSelected(true);
		int dx = 0;
		int dy = 0;
		switch (k / 10) {
		case 0:
			dy = 1;
			break;
		case 1:
			dx = 1;
			break;
		case 2:
			dx = -1;
			break;
		case 3:
			dy = -1;
			break;
		}
		f.move(dx, dy);
	}

	public long[] fetchHint() {
		long[] l = SampleUtil.toData(figures);
		if (hints != null) {
			int n = hints.length;
			for (int i = 0; i < n - 2; i += 2) {
				if (l[0] == hints[i] && l[1] == hints[i + 1]) {
					long[] res = new long[n - i - 2];
					System.arraycopy(hints, i + 2, res, 0, n - i - 2);
					return res;
				}
				if (l[0] == hints[i + 1] && l[1] == hints[i]) {
					long[] res = new long[n - i - 2];
					for (int j = i + 2; j < n; j += 2) {
						res[j - i - 2] = hints[j + 1];
						res[j - i - 1] = hints[j];
					}
					return res;
				}
			}
			hints = null;
		}
		int type = database.getGameData().getFormat();
		long[] val = SampleUtil.getSamples(type);
		for (int i = 1; i <= 5; i++) {
			SampleManager sm = new SampleManager(l[0], l[1], val, 5 * i);
			long[] res = sm.getSolution();
			if (res != null)
				return res;
		}
		return null;
	}

	
	public void showHint() {
		Runnable runnable = new Runnable() {
			public void run() {
				Runnable runnable = null;
				long[] vals = fetchHint();
				if (vals == null) {
					undo();
					runnable = new Runnable() {
						public void run() {
							invalidate();
						}
					};
				} else if (vals.length >1 ) {
					hints = vals;
					int[] res = SampleUtil.fromHint(figures, vals[0], vals[1]);
					figures[res[0]].setMarked(true);
					final Figure move=figures[res[0]];
					final int dx=res[1];
					final int dy=res[2];
					runnable = new Runnable() {
						public void run() {
							moveFigure(move,dx,dy);
						}
					};
				}
				if(runnable!=null) {
				    handler.post(runnable);
				}
			}
		};
		new Thread(runnable).start();
	}

	private Figure getSelected() {
		for (int i = 0; i < figures.length; i++) {
			if (figures[i].isSelected()) {
				return figures[i];
			}
		}
		return null;
	}

	private void select(Figure f, int dx, int dy) {
		int x1 = f.getX();
		int y1 = f.getY();
		int w = f.getWidth();
		int h = f.getHeight();
		int xx = x1;
		int yy = y1;
		if (dx < 0) {
			xx--;
		} else if (dx > 0)
			xx += w;
		else if (dy < 0)
			yy--;
		else if (dy > 0)
			yy += h;
		else
			return;
		if (xx < 0 || xx >= 4 || yy < 0 || yy >= 5)
			return;
		int[][] z = FigureFactory.mapOf(figures);
		if (f.isMarked() && z[yy][xx] == 0) {
			if (dx != 0 && (h == 1 || (yy < 4 && z[yy + 1][xx] == 0))) {
				moveFigure(f,dx, 0);
				return;
			}
			if (dy != 0 && (w == 1 || (xx < 3 && z[yy][xx + 1] == 0))) {
				moveFigure(f,0, dy);
				return;
			}
		}
		if (dx != 0 && h > 1 && z[yy + 1][xx] != 0) {
			yy++;
		} else if (dy != 0 && w > 1 && z[yy][xx + 1] != 0) {
			xx++;
		} else {
			while (z[yy][xx] == 0 || z[yy][xx] == f.getIndex() + 1) {
				if (dx < 0) {
					xx--;
				} else if (dx > 0)
					xx++;
				else if (dy < 0)
					yy--;
				else if (dy > 0)
					yy++;
				if (xx < 0 || xx >= 4 || yy < 0 || yy >= 5)
					return;
			}
		}
		int selected = z[yy][xx] - 1;
		f.setSelected(false);
		f = figures[selected];
		f.setSelected(true);
		invalidate();
	}

	private void moveFigure(Figure f, int dx, int dy) {
		animation=true;
		fly = new Fly(f, dx, dy);
		fly.setDuration(100);
		fly.setRepeatCount(5);
		startAnimation(fly);
	}

	private boolean canMove(Figure f) {
		int x = f.getX();
		int y = f.getY();
		int w = f.getWidth();
		int h = f.getHeight();
		int xx, yy;
		int[][] z = FigureFactory.mapOf(figures);
		for (int i = 0; i < 4; i++) {
			switch (i) {
			case 0:
				xx = x;
				yy = y - 1;
				break;
			case 1:
				xx = x - 1;
				yy = y;
				break;
			case 2:
				xx = x + w;
				yy = y;
				break;
			default:
				xx = x;
				yy = y + h;
				break;
			}
			if (xx >= 0 && xx < 4 && yy >= 0 && yy < 5 && z[yy][xx] == 0) {
				if (((i == 1 || i == 2) && (h == 1 || (yy < 4 && z[yy + 1][xx] == 0)))
						|| ((i == 0 || i == 3) && (w == 1 || (xx < 3 && z[yy][xx + 1] == 0)))) {
					return true;
				}
			}
		}
		return false;
	}
	
	public void stopAnimation(int index,int dx,int dy) {
		animation=false;
		fly = null;
		figures[index].move(dx, dy);
		((Database)database).newMove(index, dx, dy, clock.getTime());
		oneMove();
	}
	
	private class Fly extends Animation{
		private final static int Total=5;
		private final static float Frac=0.2f;
		private int count;
		private int dx;
		private int dy;
		private int index;

		Fly(Figure f, int dx, int dy) {
			count = 0;
			this.dx = dx;
			this.dy = dy;
			this.index = f.getIndex();
		}
		
	    @Override
	    protected void applyTransformation(float interpolatedTime, Transformation t) {
	    	++count;
	    	if(count==5) {
	    		clearAnimation();
	    		stopAnimation(index,dx,dy);
	    	}
		}
	    
	    private synchronized int getCount() {
	    	return count;
	    }

		public float[] getPosition() {
			float c=getCount()*Frac;
			return new float[] { dx*c, dy*c };
		}
	}

	public int getType() {
		return database.getGameData().getFormat();
	}


	@Override
	public Rect getBorderBounds() {
		int border=UIPreference.getBorderSpan()+2;
		int dim=figures[9].getDim();
		if(height>5*dim+2*border) {
			return new Rect(width/2-2*dim-border, 0, width/2+2*dim+border, 5*dim+2*border);
		}
		return new Rect(width/2-2*dim-border, height/2-5*dim/2-border, width/2+2*dim+border, height/2+5*dim/2+border);
	}


	@Override
	protected boolean navigate(int dx, int dy) {
		Figure f =getSelected();
		if(f!=null) {
			select(f,dx,dy);
			return true;
		}
		return false;
	}


	@Override
	protected boolean navigate() {
		Figure f = getSelected();
		if(f!=null){
			if (f.isMarked()) {
				f.setMarked(false);
				invalidate();
				return true;
			} else if (canMove(f)) {
				f.setMarked(true);
				invalidate();
				return true;
			}
		}	
		return false;
	}


	@Override
	protected int stateAfterMove() {
		return (figures[0].getX() == 1 && figures[0].getY() == 3)?Complete:Step;
	}

	@Override
	protected AbstractDatabase create(Context context) {
		return new Database(context);
	}


	@Override
	protected void clear() {
	}


	@Override
	protected boolean pointerPressed(int x, int y) {
		Figure f = getSelected();
		Figure g=null;
		LOOP: for (int j = 0; j < 2; j++) {
			for (int i = 0; i < figures.length; i++) {
				if (figures[i].contains(getResources(), x, y, j)) {
					g=figures[i];
					break LOOP;
				}
			}
		}
		if(g==null&&f!=null) { 
			if(f.contains(getResources(), x, y, 2)) {
				g=f;
			}	
		}	
		if (g!=null) {
			if(f==null||f!=g) {
				if(f!=null) f.setSelected(false);
				g.setSelected(true);
				f=g;
			}
			if (canMove(f)) {
				f.setMarked(true);
			}
			invalidate();
		}
		return g!=null;
	}

	@Override
	protected void pointerReleased(int x, int y, boolean flag) {
	}

	@Override
	protected boolean pointerDragged(int dx, int dy) {
		Figure f = getSelected();
    	 if(f!=null&&f.isMarked()) {
    		 if(Math.abs(dx)>Math.abs(dy)) {
    			 dy=0;
    			 dx=dx<0?-1:1;
    		 }
    		 else  {
    			 dx=0;
    			 dy=dy<0?-1:1;
    		 }
    		 select(f, dx, dy);
    		 return true;
    	 }
		return false;
	}

	@Override
	protected void stepDone() {
	}

	@Override
	protected void moveAround() {
	}

	@Override
	public int getMoveNumber() {
		List<Integer> mvs=((GameData)getGameData()).getMoves();
		
		return mvs==null?0:mvs.size();
	}
	
}
