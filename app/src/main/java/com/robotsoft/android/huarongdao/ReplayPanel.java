package com.robotsoft.android.huarongdao;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.robotsoft.android.base.UIPreference;

public class ReplayPanel extends Activity {
	private FigureView view;
	private ImageButton resume;
	private ImageButton pause;
	private ImageButton back;
	private ImageButton repeat;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		boolean land=replayLandScape(this);
		setContentView(replayLandScape(this) ? R.layout.replay_panel_land
				: R.layout.replay_panel);
		view = (FigureView) findViewById(R.id.replay_panel);
		resume = (ImageButton) findViewById(R.id.resume_button);
		pause = (ImageButton) findViewById(R.id.pause_button);
		back = (ImageButton) findViewById(R.id.back_button);
		repeat = (ImageButton) findViewById(R.id.repeat_button);
		LinearLayout box = (LinearLayout) findViewById(R.id.replay_screen);
		box.setBackgroundDrawable(UIPreference.getBackgroundFactory()
				.createBoard(this, view));
		playStart();
		Bundle extras = getIntent().getExtras();
		int id = extras.getInt(DBConstants.INDEX, -1);
		if (id != -1) {
			Database db = new Database(this);
			view.setStatsItem(db.getStatsItem(id), new ProgressListener() {
				@Override
				public void onFinish() {
					playDone();
				}

				@Override
				public void onBack() {
					resume.setEnabled(true);
					repeat.setEnabled(true);
					back.setEnabled(true);
				}

			});
		}
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void play(View v) {
		view.resume();
		playStart();
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void pause(View v) {
		view.clearAnimation();
		resume.setEnabled(true);
		repeat.setEnabled(true);
		back.setEnabled(true);
		pause.setEnabled(false);
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void repeat(View v) {
		view.restart();
		playStart();
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void back(View v) {
		try {
			if (view.goBack()) {
				resume.setEnabled(false);
				repeat.setEnabled(false);
				back.setEnabled(false);
				pause.setEnabled(false);
			}
		} catch (Exception ex) {
			Log.e("HRD", "Back play error: ", ex);
		}
	}

	private void playDone() {
		resume.setEnabled(false);
		back.setEnabled(false);
		repeat.setEnabled(true);
		pause.setEnabled(false);
	}

	private void playStart() {
		resume.setEnabled(false);
		repeat.setEnabled(false);
		back.setEnabled(false);
		pause.setEnabled(true);
	}

	public static boolean replayLandScape(Activity ctx) {
		Display display = ctx.getWindowManager().getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		return width > height;
	}

	public static interface ProgressListener {
		void onFinish();
		void onBack();
	}

}
