package com.robotsoft.android.huarongdao;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.robotsoft.android.huarongdao.ReplayPanel.ProgressListener;

public class ReplayAnimation extends Animation {
	private final static int Total = 5;
	private final static float Frac = 0.2f;
	private final static int Pause = 7;
	private final static int Span = Total + Pause;
	private int count;
	private StatsItem item;
	private int allSteps;
	private Figure[] figures;
	private int[][] position;
	private Figure prev;
	private ProgressListener listener;
	private Playback playback;
	private View view;

	public ReplayAnimation(View view, StatsItem item, ProgressListener listener) {
		this.view=view;
		this.item = item;
		allSteps = Span * item.getMoves().length;
		this.listener = listener;
	}

	public int getType() {
		return item.getType();
	}

	public void restart() {
		playback=null;
		count = 0;
		setDuration(100);
		setRepeatCount(allSteps);
		resetXY();
	}
	
	public void resume() {
		if (playback != null) {
			playback = null;
			int n = count / Span;	
			if(n==0) {
				restart();
			}
			else {
				forStep(n);
			}
		}
	}

	
	private void resetXY() {
		for (int i = 0; i < 10; i++) {
			figures[i].setXy(position[i][0], position[i][1]);
			figures[i].setSelected(false);
		}
	}

	public void setFigures(Figure[] figures) {
		this.figures = figures;
		position = new int[10][2];
		for (int i = 0; i < 10; i++) {
			position[i][0] = figures[i].getX();
			position[i][1] = figures[i].getY();
		}
	}

	public Animation goBack() {
		int n = count / Span;
		int m = count%Span;
		if(m<Pause-2) {
			n--;
		}
		if(n<=0) {
			playback = null;	
			return null;
		}
		Player p=forStep(n);
		count = n*Span;
		playback = new Playback(view, p.figure, p.dx, p.dy, listener);
		return playback;
	}
	
	private Player forStep(int step) {
		resetXY();
		int dx=0;
		int dy=0;
		Figure f=null;
		for(int i=0; i<step; i++) {
			int k=item.getMoves()[i];
			dx=0;
			dy=0;
			switch(k/10) {
			case 0:dy=-1;break;
			case 1:dx=-1;break;
			case 2:dx=1;break;
			case 3:dy=1;break;
			}
			f=figures[k%10];
			f.move(dx, dy);
		 }
		return new Player(f,dx,dy);
	}
	

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		int c = increasCount();
        if (c == allSteps) {
        	view.clearAnimation();
			if (listener != null)
				listener.onFinish();
		}
	}

	private synchronized int increasCount() {
		int c =++count;
		return c;
	}

	private synchronized int getCount() {
		return count;
	}

	public Figure getCurrent() {
		if(playback!=null) {
			return playback.getFigure();
		}
		int n = getCount();
		int p = n % Span;
		if (p < Pause) {
			if (p == Pause - 2) {
				if (prev != null)
					prev.setSelected(false);
				int s = item.getMoves()[n / Span];
				prev = figures[s % 10];
				prev.setMarked(true);
			}
			return null;
		}
		return prev;
	}

	public float[] getPosition() {
		if(playback!=null) {
			return playback.getPosition();
		}
		int n = getCount();
		int p = n % Span;
		int s = item.getMoves()[n / Span];
		if (p < Pause) {
			return null;
		}
		int dx = 0;
		int dy = 0;
		if (s < 10)
			dy = -1;
		else if (s < 20)
			dx = -1;
		else if (s < 30)
			dx = 1;
		else
			dy = 1;
		if (p == Span - 1) {
			Figure f = figures[s % 10];
			f.move(dx, dy);
			f.setMarked(true);
			return new float[] { -dx * Frac, -dy * Frac };
		}
		float c = (p - Pause) * Frac;
		return new float[] { dx * c, dy * c };
	}
	
	static class Player {
		Figure figure;
		int dx;
		int dy;
		public Player(Figure figure, int dx, int dy) {
			this.figure = figure;
			this.dx = dx;
			this.dy = dy;
		}
	}
}
