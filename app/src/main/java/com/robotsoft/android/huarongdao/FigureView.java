package com.robotsoft.android.huarongdao;

import com.robotsoft.android.base.HasBorder;
import com.robotsoft.android.base.UIPreference;
import com.robotsoft.android.huarongdao.ReplayPanel.ProgressListener;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;

public class FigureView extends View implements HasBorder {
	private Figure[] figures;
	private ReplayAnimation anim;
	private int width;
	private int height;

	public FigureView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public FigureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public FigureView(Context context) {
		super(context);
		init();
	}
	
	private void init() {
		setFocusable(true);
		setFocusableInTouchMode(true);
		requestFocus();
	}
	
	private void initFigures() {
		figures= FigureFactory.initFigures(anim.getType(), width, height, getContext().getResources());
		anim.setFigures(figures);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    int w = MeasureSpec.getSize(widthMeasureSpec);
	    int h = MeasureSpec.getSize(heightMeasureSpec);
	    if(w>h) {
	    	width=w-90;
	    	height=h;
	    }
	    else {
	    	width=w;
	    	height=(h>450)?h-52:(h>350)?h-42:h-32;
	    }
	    initFigures();
	    setMeasuredDimension(width, height);
	    restart();
	}
	
	public void restart() {
		anim.restart();
		startAnimation(anim);
	}
	
	public void resume() {
		anim.resume();
		startAnimation(anim);
	}

	
	public boolean goBack() {
		Animation a=anim.goBack();
		Log.d("HRD","a="+a);
		if(a==null)return false;
		startAnimation(a);
		return true;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		if (figures != null) {
			try{
			Figure f=anim.getCurrent();
			float[] p=f==null?null:anim.getPosition();
			for (int i = 0; i < figures.length; i++) {
				if (f==null||!figures[i].equals(f)) {
					figures[i].paint(getResources(),canvas);
				}
			}
			if(f!=null) {
				f.paint(getResources(),canvas, p);
			}
			}catch(Throwable ex) {
				Log.e("HRD","Draw Error",ex);
			}
		}
	}

	public void setStatsItem(StatsItem statsItem, ProgressListener listener) {
		anim=new ReplayAnimation(this,statsItem, listener);
	}

	@Override
	public Rect getBorderBounds() {
		if(figures==null) return null;
		int border=UIPreference.getBorderSpan()+2;
		int dim=figures[9].getDim();
		if(height>5*dim+2*border) {
			return new Rect(width/2-2*dim-border, 0, width/2+2*dim+border, 5*dim+2*border);
		}
		return new Rect(width/2-2*dim-border, height/2-5*dim/2-border, width/2+2*dim+border, height/2+5*dim/2+border);
	}

}
