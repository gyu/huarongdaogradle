package com.robotsoft.android.huarongdao;

import com.robotsoft.android.base.AbstractView;
import com.robotsoft.android.base.DefaultBackgroundFactory;
import com.robotsoft.android.base.Game;
import com.robotsoft.android.base.UIPreference;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class Huarongdao extends Game {

	@Override
	protected void mapUIPreference() {
		float scale = getResources().getDisplayMetrics().density;
		if(scale>1.5f) scale=1.5f;
		UIPreference.setBorderSpan((int)(30*scale/3+0.5));
		//UIPreference.setBorderSpan(14);
    	UIPreference.setFrameSpan(10);
    	UIPreference.setBackground(R.drawable.background);
    	UIPreference.setFrame(R.drawable.frame);
    	UIPreference.setBackgroundFactory(new BackgroundFactoryImpl());
    	UIPreference.setNewGameClass(OptionPanel.class);
    	UIPreference.setStatsClass(StatsPanel.class);
	}

	@Override
	protected AbstractView createView() {
		try {
		return new HuarongdaoView(this);
		}catch(Throwable ex) {
			Log.e("HRD","Error here",ex);
			return null;
		}
	}

	@Override
	protected void processOption(int resultCode, Intent data) {
		switch(resultCode) {
		case OptionPanel.Apply: 
			Bundle bundle=data.getExtras();
			int seed=bundle.getInt(ShufflePanel.Seed, -1);
			if(seed!=-1) {
				((Database)view.getDatabase()).doShuffle(seed, bundle.getInt(ShufflePanel.Number, 500));
				view.start();
			}
			break;
		case OptionPanel.Shuffle:
	    	Intent intnt = new Intent(this, ShufflePanel.class);
			startActivityForResult(intnt, InputOption);
			break;
		case OptionPanel.Type:
	    	Intent intent = new Intent(this, OptionPanel.class);
			intent.putExtra( UIPreference.Claz, R.layout.game_type);
			intent.putExtra( UIPreference.Component, R.id.game_type);
			startActivityForResult(intent, InputOption);
		break;
		case OptionPanel.Type1: changeType(0);break;
		case OptionPanel.Type2:changeType(1);break;
		case OptionPanel.Type3:changeType(2);break;
		case OptionPanel.Type4:changeType(3);break;
		}
	}

	
	private void changeType(int type) {
		((Database)view.getDatabase()).typeChanged(type);
		view.start();
	}

	@Override
	protected void processBundle(Bundle bundle) {
	}
}