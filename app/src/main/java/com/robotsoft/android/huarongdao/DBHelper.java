package com.robotsoft.android.huarongdao;

import static android.provider.BaseColumns._ID;
import static com.robotsoft.android.huarongdao.DBConstants.*;

import com.robotsoft.android.base.AbstractDBHelper;

import android.content.ContentValues;
import android.content.Context;

public class DBHelper extends AbstractDBHelper {
	private static final String DATABASE_NAME = "huarongdao.db";

	public DBHelper(Context context) {
		super(context, DATABASE_NAME);
	}

	@Override
	protected String statsSql(String table) {
		return "CREATE TABLE " + STATS_TABLE_NAME + " ("
		+ _ID  + " INTEGER PRIMARY KEY AUTOINCREMENT,"
		+ TYPE + " INTEGER,"
		+ TIME + " INTEGER,"
		+ LENGTH+" INTEGER," 
		+ DATA + "  BLOB);";
	}

	@Override
	protected String gameSql(String table) {
		return "CREATE TABLE " + GAME_TABLE_NAME + " ("
		+ TYPE + " INTEGER,"
		+ TIME + " INTEGER,"
		+ SEED + " INTEGER,"
		+ LOOP + " INTEGER,"
		+ MUSIC +" INTEGER);";
	}

	@Override
	protected String figureSql(String table) {
		return "CREATE TABLE " + table + " ("
		+ INDEX + " INTEGER,"
		+ MOVE +" INTEGER);";
	}

	@Override
	protected String recentSql(String table) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ContentValues initGame() {
		ContentValues values = new ContentValues();
	    values.put(TYPE, 0);
	    values.put(TIME, 0);
	    values.put(SEED, -1);
	    values.put(LOOP, -1);
	    values.put(MUSIC, 1);
	    return values;
	  }

}
