package com.robotsoft.android.huarongdao;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import com.robotsoft.android.base.Background;
import com.robotsoft.android.base.BackgroundFactory;
import com.robotsoft.android.base.DefaultBackground;
import com.robotsoft.android.base.DlgFrame;
import com.robotsoft.android.base.HasBorder;
import com.robotsoft.android.base.ImageDef;
import com.robotsoft.android.base.UIPreference;

public class BackgroundFactoryImpl implements BackgroundFactory {

	public Drawable createBoard(Context context,HasBorder hasBorder) {
		return new GameBoard(context,hasBorder);
	}
	
	@Override
	public Drawable createFrame(Context context) {
		return new DlgFrame(context, UIPreference.getBackground(),UIPreference.getBorderSpan(), UIPreference.getFrame(),UIPreference.getFrameSpan());
	}
	
	public Drawable createBackground(Context context) {
		return new DefaultBackground(context,UIPreference.getBackground(),UIPreference.getBorderSpan());
	}
	
	public Drawable createFill(Context context) {
		return new FillBackground(context,UIPreference.getBackground(),UIPreference.getBorderSpan());
	}


	static class GameBoard extends Background {
		private HasBorder hasBorder;
		GameBoard(Context context,HasBorder hasBorder) {
			super(context,  UIPreference.getBackground(), UIPreference.getBorderSpan());
			this.hasBorder=hasBorder;
		}
		
		protected void borderBottom(Canvas g, int left, int bottom, int width, int border, ImageDef def) {
			int l = (width - 2 * border)/4 + 2;
			int w = def.getW();
			int n = l / w;
			int x = left + border;
			for (int i = 0; i < n; i++) {
				g.drawBitmap(def.getData(), 0, w, x, bottom- border, w, border, false, null);
				x += w;
			}
			g.drawBitmap(def.getData(), 0, w, x, bottom - border, l - n * w, border, false, null);
			x = left + width - border - l;
			for (int i = 0; i < n; i++) {
				g.drawBitmap(def.getData(), 0, w, x, bottom- border, w, border, false, null);
				x += w;
			}
			g.drawBitmap(def.getData(), 0, w, x, bottom - border, l - n * w, border, false, null);
		}

		@Override
		protected Rect borderBounds() {
			return hasBorder.getBorderBounds();
		}
	}
	
	static class FillBackground extends Background {
		
		public FillBackground(Context ctx, int resId, int border) {
			super(ctx, resId, border);
		}

		@Override
		protected Rect borderBounds() {
			return null;
		}

	}



}
