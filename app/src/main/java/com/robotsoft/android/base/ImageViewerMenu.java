package com.robotsoft.android.base;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.robotsoft.android.huarongdao.R;

public class ImageViewerMenu extends Activity {
	public static final String RestoreKey = "restore";
	public static final String OriginalKey = "original";
	public static final String FitKey = "fit";
	public static final String SaveKey = "save";
	public static final int Restore = 0;
	public static final int RotateLeft = 1;
	public static final int RotateRight = 2;
	public static final int Flip = 3;
	public static final int Original = 4;
	public static final int Fit = 5;
	public static final int Save = 6;
	public static final int Close = 7;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			Bundle extras = getIntent().getExtras();
			setContentView(R.layout.image_viewer_menu);
			TableLayout table = (TableLayout) findViewById(R.id.image_viewer_menu);
			if (extras.getBoolean(RestoreKey, false)) {
				remove(table,R.id.rotate_left);
				remove(table,R.id.rotate_right);
				remove(table,R.id.flip);
			} else {
				remove(table,R.id.img_restore);
			}
			if (!extras.getBoolean(OriginalKey,false)) {
				remove(table, R.id.img_original);
			} 
			if (!extras.getBoolean(FitKey,false)) {
				remove(table, R.id.img_fit);
			} 
			if (!extras.getBoolean(SaveKey)) {
				remove(table, R.id.img_save);
			}
			BackgroundFactory bf = UIPreference.getBackgroundFactory();
			table.setBackgroundDrawable(bf.createFrame(this));
		} catch (Exception ex) {
			Log.e("RobotSoft", "Fail to create Image Menu", ex);
		}
	}

	private void remove(TableLayout table, int rowId) {
		TableRow row = (TableRow) findViewById(rowId);
		Log.d("RobotSoft", "Remove " + row);
		table.removeView(row);
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void restore(View v) {
		done(Restore);
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void rotateLeft(View v) {
		done(RotateLeft);
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void rotateRight(View v) {
		done(RotateRight);
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void flip(View v) {
		done(Flip);
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void fit(View v) {
		done(Fit);
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void original(View v) {
		done(Original);
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void save(View v) {
		done(Save);
	}

	@SuppressWarnings({ "UnusedDeclaration" })
	public void exit(View v) {
		done(Close);
	}

	private void done(int code) {
		setResult(code);
		finish();
	}

}
