package com.robotsoft.android.base;

public class ImageDef {
	private int w;
	private int h;
	private int[] data;
	public ImageDef(int w, int h, int[] data) {
		this.w = w;
		this.h = h;
		this.data = data;
	}
	public int getW() {
		return w;
	}
	public int getH() {
		return h;
	}
	public int[] getData() {
		return data;
	}
}
