package com.robotsoft.android.base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.robotsoft.android.huarongdao.R;

public abstract class Game extends Activity {
	protected static final int AskOption=0;
	protected static final int InputOption=1;
	protected AbstractView view;
	protected boolean busy=false;
	protected abstract void mapUIPreference();
	protected abstract AbstractView createView();
	protected abstract void processOption(int resultCode, Intent data);
	protected abstract void processBundle(Bundle bundle);
	private static final int ShowHint=102;
	private static final int ShowClock=106;
	private static final int Undo=110;
	private static final int Help=112;
	private static final int PlayMusic=114;
	private static final int Preference=116;
	
	
	public Game() {
	}

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        String[] menus=getResources().getStringArray(R.array.menus);
        int[] menuIds=getResources().getIntArray(R.array.menuAction);
        for(int i=0; i<menus.length; i++) {
        	String title;
        	if(menuIds[i]==ShowClock) {
        		title=getResources().getString(view.isShowClock()? 
        				R.string.hide_clock: R.string.show_clock);
        	}
        	else if(menuIds[i]==PlayMusic) {
        		title=getResources().getString(Media.isPlaying()? 
            		R.string.music_off: R.string.music_on);
        	}else {
        		title=menus[i];
        	}
        	menu.add(0, Menu.FIRST + i, 0, title);
        }
        return true;
    }
    
    protected View viewWrapper(View v) {
    	return v;
    }
    
    protected void postAttach() {
    }

	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mapUIPreference();
        Log.d("RobotSoft","Start Game");
    	requestWindowFeature(Window.FEATURE_NO_TITLE);
		try {
			view = createView();
			setContentView(viewWrapper(view));
			postAttach();
			if (Media.canPlay()) {
				if (view.getGameData().isPlayMusic()) {
					Media.start(this);
				}
			}
		} catch (Throwable ex) {
			Log.e("RobotSoft", "Launch error", ex);
		}
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int[] menus=getResources().getIntArray(R.array.menuAction);
		switch(menus[item.getItemId()-Menu.FIRST]) {
		case PlayMusic:
			if(Media.isPlaying()) {
				Media.stop();
				item.setTitle(R.string.music_on);
			}
			else {
				Media.start(this);
				item.setTitle(R.string.music_off);
			}
			view.getDatabase().toggleMusic();
			return true;
		case ShowClock:
			item.setTitle(view.isShowClock()? R.string.show_clock: R.string.hide_clock);
			view.resetShowClock();
			return true;
		case ShowHint:
			showHint();
			return true;
		case Undo:
			view.undo();
			return true;
		case Help:
			showHelp();
			return true;
		case Preference:
			managePreference();
			return true;
			
		}
		return false;
	}
	
	
	
	private void managePreference() {
		// TODO Auto-generated method stub
		
	}
	protected void showHint() {
		view.showHint();
	}

    @Override
    public void onBackPressed() {
    	showOptionDialog();
    }
    
	public void showOptionDialog() {
		if (!busy) {
			try {
				busy = true;
				Intent intent = new Intent(this, MenuPanel.class);
				startActivityForResult(intent, AskOption);
			} catch (Throwable ex) {
				Log.e("RobotSoft", "Fail to show option dialog", ex);
			}
		}
	}
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode,
		Intent data) {
        if (requestCode == AskOption) {
        	busy=false;
        	if(MenuPanel.NewGame==resultCode) {
        		showNewGameMenu();
        	}
        	else if(MenuPanel.Exit==resultCode) {
        		Media.stop();
        		doClear();
        		finish();
        	}
        }
        else if (MenuPanel.Start==resultCode) {
        	view.start();
        }
        else if(MenuPanel.NewGame==resultCode) {
        	Bundle bundle=(data==null)?null: data.getExtras();
        	if(bundle!=null) {
        		processBundle(bundle);
        	}
    		showNewGameMenu();
    	}
        else {
        	processOption(resultCode, data);
        }
    }
    
    protected void showNewGameMenu() {
    	Intent intent = new Intent(this, UIPreference.getNewGameClass());
		intent.putExtra( UIPreference.Claz, R.layout.new_game);
		intent.putExtra( UIPreference.Component,R.id.new_game);
		startActivityForResult(intent, InputOption);
    }
    
    private void doClear() {
    	try {
        	ImageFactory fact=UIPreference.getImageFactory();
        	if(fact!=null) fact.clear();
        	view.doClear();
       	}catch(Throwable t) {
       		Log.e("RobotSoft", "Failed to Finish",t);
       	}
    }

    protected void showHelp() {
    	Intent intent = new Intent(this, HelpPage.class);
		startActivity(intent);
    }
    
}
