package com.robotsoft.android.base;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

public abstract class Background extends Drawable {
	protected ImageDef[] defData;
	private Bitmap bmp;
	protected int width;
	protected int height;
	
	public Background(Context ctx, int resId, int border) {
		defData = getBackgroundData(ctx, border, resId);
	}

	protected abstract Rect borderBounds();

	protected ImageDef[] frameData() {
		if(defData.length<=4) return null;
		ImageDef[] data = new ImageDef[8];
		for(int i=0; i<8; i++) {
			data[i]=defData[i+4];
		}
		return data;
	}

	protected void borderTop(Canvas g, int left, int top, int width, int border, ImageDef def) {
		int l = width - 2 * border;
		int w = def.getW();
		int n = l / w;
		int x = left + border;
		for (int i = 0; i < n; i++) {
			g.drawBitmap(def.getData(), 0, w, x, top, w,
					border, false, null);
			x += w;
		}
		g.drawBitmap(def.getData(), 0, w, x, top, l - n * w, border, false, null);
	}
	
	protected void borderBottom(Canvas g, int left, int bottom, int width, int border, ImageDef def) {
	
	int l =width - 2 * border;
	int w = def.getW();
	int n = l / w;
	int x = left + border;
	for (int i = 0; i < n; i++) {
		g.drawBitmap(def.getData(), 0, w, x, bottom- border, w, border, false, null);
		x += w;
	}
	g.drawBitmap(def.getData(), 0, w, x, bottom - border, l
			- n * w, border, false, null);
	}

	
	private Bitmap createBitmap() {
		Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_4444);
		Canvas canvas = new Canvas(bmp);
		int a = defData[0].getW();
		int b = defData[0].getH();
		int n = width / a;
		if (width % a > 0)
			n++;
		int m = height / b;
		if (height % b > 0)
			m++;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				ImageDef def = defData[(i % 2) + (j % 2) * 2];
				canvas.drawBitmap(def.getData(), 0, a, i * a, j * b, a, b,
						false, null);
			}
		}
		ImageDef[] bd=frameData();
		Rect rect =(bd==null)?null: borderBounds();
		if(rect!=null){
			int border=bd[0].getW();
			if (rect.left + border > 0) {
				if (rect.top + border > 0) {
					canvas.drawBitmap(bd[0].getData(), 0, border,
							rect.left, rect.top, border, border, false, null);
				}
				if (rect.top + rect.height() < height + border) {
					canvas.drawBitmap(bd[1].getData(), 0, border,
							rect.left, rect.bottom - border, border, border,
							false, null);
				}
				int l = rect.height() - 2 * border;
				ImageDef d = bd[5];
				int h = d.getH();
				n = l / h;
				int y = rect.top + border;
				for (int i = 0; i < n; i++) {
					canvas.drawBitmap(d.getData(), 0, border, rect.left, y,
							border, h, false, null);
					y += h;
				}
				canvas.drawBitmap(d.getData(), 0, border, rect.left, y, border,
						l - n * h, false, null);
			}
			if (rect.top + border > 0) {
				borderTop(canvas, rect.left, rect.top, rect.width(), border, bd[4]);
			}
			if (rect.right < width + border) {
				if (rect.top + border > 0) {
					canvas.drawBitmap(bd[2].getData(), 0, border,
							rect.right - border, rect.top, border, border,
							false, null);
				}
				if (rect.top + rect.height() - border < height) {
					canvas.drawBitmap(bd[3].getData(), 0, border,
							rect.right - border, rect.bottom - border, border,
							border, false, null);
				}
				int l = rect.height() - 2 * border;
				ImageDef d = bd[7];
				int h = d.getH();
				n = l / h;
				int y = rect.top + border;
				for (int i = 0; i < n; i++) {
					canvas.drawBitmap(d.getData(), 0, border, rect.right
							- border, y, border, h, false, null);
					y += h;
				}
				canvas.drawBitmap(d.getData(), 0, border, rect.right - border,
						y, border, l - n * h, false, null);
			}
			if (rect.bottom < height + border) {
				borderBottom(canvas, rect.left, rect.bottom, rect.width(), border, bd[6]);
			}
		}
		return bmp;
		
	}
	
	@Override
	public void draw(Canvas g) {
		if(bmp==null) {
			bmp=createBitmap();
		}
		g.drawBitmap(bmp, 0, 0, null);
	}
	
	private ImageDef[] getBackgroundData(Context ctx, int border, int resId) {
		ImageDef[] defData = new ImageDef[border <= 0 ? 4 : 12];
		Bitmap p = BitmapFactory.decodeResource(ctx.getResources(), resId);
		int w = p.getWidth();
		int h = p.getHeight();
		int x = w - border * 2;
		int y = h - border * 2;
		int[] d00 = new int[x * y];
		p.getPixels(d00, 0, x, border, border, x, y);
		defData[0] = new ImageDef(x, y, d00);
		int[] d10 = new int[x * y];
		for (int i = 0, k = 0; i < y; i++) {
			for (int j = 0, l = i * x + x - 1; j < x; j++, k++, l--) {
				d10[k] = d00[l];
			}
		}
		defData[1] = new ImageDef(x, y, d10);
		for (int i = 0; i < 2; i++) {
			int[] s = (i == 0) ? d00 : d10;
			int[] d = new int[x * y];
			for (int j = 0; j < y; j++) {
				System.arraycopy(s, (y - 1 - j) * x, d, j * x, x);
			}
			defData[i + 2] = new ImageDef(x, y, d);
		}
		if (border > 0) {
			ImageDef[] q=frameData(p, border);
			for(int i=0; i<8; i++)defData[i+4]=q[i];
		}
		return defData;
	}
	
	protected ImageDef[] frameData(Bitmap bmp, int border) {
		ImageDef[] imgs = new ImageDef[8];
		int w=bmp.getWidth();
		int h=bmp.getHeight();
		for (int i = 0; i < 4; i++) {
			int[] data = new int[border * border];
			bmp.getPixels(data, 0, border, (i < 2) ? 0 : w - border,
					(i == 0 || i == 2) ? 0 : h - border, border, border);
			imgs[i] = new ImageDef(border, border, data);
		}
		for (int i = 0; i < 4; i++) {
			int f = (i == 1 || i == 3) ? border : w - 2 * border;
			int g = (i == 0 || i == 2) ? border : h - 2 * border;
			int x = (i == 0 || i == 2) ? border : i == 1 ? 0 : w - border;
			int y = (i == 1 || i == 3) ? border : i == 0 ? 0 : h - border;
			int[] data = new int[f * g];
			bmp.getPixels(data, 0, f, x, y, f, g);
			imgs[i + 4] = new ImageDef(f, g, data);
		}
		return imgs;
	}

	@Override
	public int getOpacity() {
		return 0;
	}

	@Override
	public void setAlpha(int alpha) {
	}

	@Override
	public void setColorFilter(ColorFilter cf) {
	}
	
	public int[] crop(int x,int y,int w,int h) {
		if(bmp==null||bmp.getWidth()<x+w||bmp.getHeight()<y+h) {
			return null;
		}
		int[] d=new int[w*h];
		bmp.getPixels(d, 0, w, x, y, w, h);
		return d;
	}

	@Override
	public void setBounds(int left, int top, int right, int bottom) {
		doSet(left, top, right, bottom);
		super.setBounds(left, top, right, bottom);
	}

	@Override
	public void setBounds(Rect bounds) {
		doSet(bounds.left, bounds.top, bounds.right, bounds.bottom);
		super.setBounds(bounds);
	}
	
	private void doSet(int left, int top, int right, int bottom) {
		int w=right-left;
		int h=bottom-top;
		if(w!=width||height!=h) {
			width=right-left;
			height=bottom-top;
			bmp=null;
		}
	}

}
