package com.robotsoft.android.base;

public class AbstractGameData {
	private int format;
	private int timePlayed;
	private boolean playMusic;

	public int getFormat() {
		return format;
	}
	public void setFormat(int format) {
		this.format = format;
	}
	public int getTimePlayed() {
		return timePlayed;
	}
	public void setTimePlayed(int timePlayed) {
		this.timePlayed = timePlayed;
	}
	public boolean isPlayMusic() {
		return playMusic;
	}
	public void setPlayMusic(boolean playMusic) {
		this.playMusic = playMusic;
	}
	
}
