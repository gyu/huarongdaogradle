package com.robotsoft.android.base;

import android.content.Context;

public interface ImageFactory {
	ImageDef getImage();
	ImageDef getImage(Context ctx, String path);
	ImageDef getImage(Context ctx, String path, int format, int w, int h);
	void setCache(ImageDef def);
	ImageDef getCache();
	void clear();
	
}
