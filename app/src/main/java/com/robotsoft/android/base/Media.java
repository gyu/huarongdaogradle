package com.robotsoft.android.base;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import com.robotsoft.android.huarongdao.R;

public class Media {
	private static MediaPlayer player;
	
	public static void step(Context context) {
		play(context, R.raw.bar, false);
	}

	public static void done(Context context) {
		play(context, R.raw.fanfare, false);
	}
	
	private static void play(Context context, int resId, boolean loop) {
		try {
			Log.d("RobotSoft", "Play Sound");
			MediaPlayer	player = MediaPlayer.create(context, resId);
			player.setLooping(loop);
			player.start();
		}catch(Exception ex) {
			Log.e("RobotSoft", "Cannot create media player",ex);
		}
	}

	public static boolean isPlaying() {
		return player!=null;
	}

	public static boolean canPlay() {
		return true;
	}
	
	public static void start(final Context context) {
			Runnable runnable=new Runnable(){
				@Override
				public void run() {
					try {
						player = MediaPlayer.create(context, R.raw.bgm);
						player.setLooping(true);
						player.start();

					}catch(Exception ex) {
						player=null;
					}
				}};
				new Thread(runnable).start();
	}
	
	public static void stop() {
		if(player!=null) {
			player.stop();
			player=null;
		}
	}


}
