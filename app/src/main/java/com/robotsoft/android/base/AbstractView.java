package com.robotsoft.android.base;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.robotsoft.android.huarongdao.R;

public abstract class AbstractView extends View implements HasBorder {
	protected int width=-1;
	protected int height=-1;
	protected int ox;
	protected int oy;
	protected Clock clock;
	protected final int Complete = 0;
	protected final int Step = 1;
	protected final int Move = 2;
	protected final int Wait = 3;
	protected AbstractDatabase database;
	protected Game main;
	protected Handler handler;
	protected boolean showClock = true;
	private int startX;
	private int startY;
	protected boolean animation;
	protected boolean done;
	private boolean update;
	private boolean exit;
	private int movement=UIPreference.getGestureThreshold(getResources());
	
	
	public AbstractView(Context context) {
		this(context, true);
	}

	public AbstractView(final Context context,boolean update) {
		super(context);
		this.update=update;
		exit=false;
		database = create(context);
		main = (Game) context;
		handler = new Handler();
		setFocusable(true);
		setFocusableInTouchMode(true);
		requestFocus();
	}
	
	public abstract void showHint();

	public abstract Rect getBorderBounds();

	public abstract void undo();

	protected abstract void doInit();

	protected abstract boolean navigate(int dx, int dy);

	protected abstract boolean navigate();

	protected abstract int stateAfterMove();

	protected abstract AbstractDatabase create(Context context);
	protected abstract void doDraw(Canvas canvas);
	protected abstract void clear();
	
	protected void doClear() {
		exit=true;
		clear();
	}
	
	protected abstract boolean pointerPressed(int x, int y);
	protected abstract void pointerReleased(int x, int y, boolean flag);
	protected abstract boolean pointerDragged(int dx, int dy);
	protected abstract void stepDone();
	protected abstract void moveAround();
	

	public AbstractDatabase getDatabase() {
		return database;
	}

	public AbstractGameData getGameData() {
		return database==null?null:database.getGameData();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		int w = getMeasuredWidth();
		int h = getMeasuredHeight();
		Log.d("GameLib","onMeasure "+w+" "+h+",  "+width+" "+height);
		if (width == -1 || width != w || height!=h ) {
			initView(w, h);
		}
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		if(exit) return;
		if (width == -1 || width != w || height!=h) {
			initView(w, h);
		}
		super.onSizeChanged(w, h, oldw, oldh);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (!exit&&!animation && !done) {
			switch (keyCode) {
			case KeyEvent.KEYCODE_DPAD_UP:
				if (navigate(0, -1))
					return true;
				break;
			case KeyEvent.KEYCODE_DPAD_DOWN:
				if (navigate(0, 1))
					return true;
				break;
			case KeyEvent.KEYCODE_DPAD_LEFT:
				if (navigate(-1, 0))
					return true;
				break;
			case KeyEvent.KEYCODE_DPAD_RIGHT:
				if (navigate(1, 0))
					return true;
				break;
			case KeyEvent.KEYCODE_ENTER:
			case KeyEvent.KEYCODE_DPAD_CENTER:
				if (navigate())
					return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}


	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!exit&&!animation && !done) {
			int x = (int) event.getX();
			int y = (int) event.getY();
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				if (pointerPressed(x, y)) {
					startX = x;
					startY = y;
				} else {
					startX = -1;
					startY = -1;
				}
				break;
			case MotionEvent.ACTION_MOVE:
				boolean b = startX >= 0 && startY >= 0;
				int dx = b ? x - startX : 0;
				int dy = b ? y - startY : 0;
				if (Math.abs(dx)+Math.abs(dy)>movement) {
					if (pointerDragged(dx, dy)) {
						startX = -1;
						startY = -1;
					}
					else {
						startX = x;
						startY = y;
					}
				}
				break;
			case MotionEvent.ACTION_UP:
				b = startX >= 0 && startY >= 0;
				if (b) {
					x = startX;
					y = startY;
				}
				startX = -1;
				startY = -1;
				pointerReleased(x, y, b);
				break;
			}
			return true;
		}
		return super.onTouchEvent(event);
	}

	private void initView(int w, int h) {
		Log.d("GameLib","Init View");
		width = w;
		height = h;
		if (clock != null) {
			clock.stop();
		}
		doBackground();
		doStart();
	}
	
	public void start() {
		database.refresh();
		doStart();
	}
	
	private void doStart() {
		done = false;
		if (clock != null)
			clock.stop();
		doInit();
		postInit();
	}
	
	protected void doBackground() {
		BackgroundFactory bf = UIPreference.getBackgroundFactory();
		Background b = (Background) bf.createBoard(getContext(), this);
		setBackgroundDrawable(b);
	}
	
	protected void postInit() {
		Drawable drawable=getBackground();
		Background b=drawable!=null&&(drawable instanceof Background)?(Background)drawable:null;
		clock = new Clock(this, b);
		clock.start(getTimePlayed());
		database.reload();
	}

	public int getOx() {
		return ox;
	}

	public int getOy() {
		return oy;
	}
	
	private void doEndSession() {
		done = true;
		stepDone();
		database.gameDone();
		if (clock != null) {
			clock.gameDone();
		}
		LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		View view = vi.inflate(R.layout.congratulation_panel, null);
		final Toast toast = new Toast(getContext());
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP, 0, 100);
		toast.show();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				toast.cancel();
				main.showOptionDialog();
			}
		}, 10000);
		//this.setAnimation(animation)''
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (!exit) {
			doDraw(canvas);
			if (clock != null && showClock) {
				clock.paint(canvas, width, getContext());
			}
			drawMessage(canvas);
		}
	}

	protected void drawMessage(Canvas canvas) {
		
	}

	public boolean isShowClock() {
		return showClock;
	}

	public void resetShowClock() {
		showClock = !showClock;
		invalidate();
	}

	public int getTimePlayed() {
		return database.getGameData().getTimePlayed();
	}
	
	protected void oneMove() {
		oneMove(stateAfterMove());
	}
	
	protected void oneMove(int res) {
		if (res == Complete) {
			doEndSession();
			Media.done(getContext());			
		} else if (res == Step) {
			stepDone();
			Media.step(getContext());
		}
		else if (res == Move){
			moveAround();			
		}
	}

	protected void moveIt() {
		Runnable runnable = new Runnable() {
			public void run() {
				invalidate();
			}
		};
		handler.post(runnable);
	}

	public int theHeight() {
		return height;
	}
	
	public int getMoveNumber() {
		return -1;
	}
	
	
	
	
	
}
