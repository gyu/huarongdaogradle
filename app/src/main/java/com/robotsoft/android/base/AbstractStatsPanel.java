package com.robotsoft.android.base;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.robotsoft.android.huarongdao.R;

public abstract class AbstractStatsPanel extends Activity {

	protected AbstractStatsItem[] items;
	private AbstractDatabase database;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		database=createDatabase();
		try {
			setContentView(R.layout.stats_panel);
			items = database.getStats();
			populate((TableLayout) findViewById(R.id.stats_table));
			LinearLayout screen = (LinearLayout) findViewById(R.id.stats_screen);
			BackgroundFactory bf = UIPreference.getBackgroundFactory();
			screen.setBackgroundDrawable(bf.createBackground(this));
		} catch (Exception ex) {
			Log.d("RobotSoft", "Stats panel failure", ex);
		}
	}
	
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, Menu.FIRST, 0, getResources().getString(R.string.clear));
        menu.add(0, Menu.FIRST+1, 0, getResources().getString(R.string.close));
        return true;
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int k=item.getItemId()-Menu.FIRST;
		if(k==0) {
			database.clearStats();
		}
		finish();
		return true;
	}
	
	protected void populate(TableLayout table) {
		for (int i = 0, n = items.length; i < n; i++) {
			TableRow rw = content(items[i]);
			table.addView(rw);
		}
	}
	
	public void clear(View view) {
		database.clearStats();
		finish();
	}
	
	public void close(View view) {
		finish();
	}

	protected abstract AbstractDatabase createDatabase();
	protected abstract TableRow content(AbstractStatsItem item);

}
