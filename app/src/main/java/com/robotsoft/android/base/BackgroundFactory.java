package com.robotsoft.android.base;

import android.content.Context;
import android.graphics.drawable.Drawable;


public interface BackgroundFactory {
	Drawable createBoard(Context context, HasBorder hasBorder);
	Drawable createFrame(Context context);
	Drawable createBackground(Context context);
	Drawable createFill(Context context);
}
